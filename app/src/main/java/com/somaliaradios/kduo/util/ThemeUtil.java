package com.somaliaradios.kduo.util;


import com.somaliaradios.kduo.R;

import java.util.ArrayList;

/**
 * Created by Pankaj on 12-11-2017.
 */

public class ThemeUtil {
    public static final int THEME_NORMAL = 0;
    public static final int THEME_BLACK = 1;

    public static int getThemeId(int theme){
        int themeId=0;
        switch (theme){
            case THEME_NORMAL  :
                themeId = R.style.TransistorAppTheme;
                break;
            case THEME_BLACK  :
                themeId = R.style.ThemeDark;
                break;
            default:
                break;
        }
        return themeId;
    }

}
